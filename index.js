const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validateEmail(email){
    return emailRegex.test(email);
}

function submitMail(){
    var emailInput = $("#emailInput").val();
    if(emailInput!=null && emailInput.length > 0){
        if(validateEmail(emailInput)){
            postEmail(emailInput); 
        }
        else{
            $("#emailError").show();
        }
    }
    else{
        $("#emailError").show();
    }
}

function postEmail(email){
    $("#emailError").hide();
    $(".loader-section").show();
    $(".button-text").hide();
    var data = {
        email: email
    }
    $.post("https://rquero.herokuapp.com/addemail", data)
    .done(function(result){
        $(".invitation-section").hide();
        $("#emailSuccessMessage").show();
    })
    .fail(function(){
        $("#emailError").hide();
        $(".loader-section").hide();
        $(".button-text").show();
        alert("Something went wrong. Check your connection and try again!");
    })
}